<?php

namespace Drupal\dismissableblocks\Service;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Component\Utility\Html;
use Drupal\dismissableblocks\Constants\DismissableBlocksConstants;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountProxy;

const DISMISSABLE_BLOCKS_PERMISSION = 'configure dismissable blocks';

/**
 * Dismissable Block Service Class.
 */
class DismissableBlockHelperService {

  use StringTranslationTrait;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Path Matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The block entity.
   *
   * @var useDrupal\block\Entity\Block
   */
  protected $blockEntity;

  /**
   * Construct of Block Class service.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, Connection $database, RequestStack $request_stack, PathMatcherInterface $path_matcher, UuidInterface $uuid_service, AliasManagerInterface $alias_manager, CurrentPathStack $current_path, EntityRepositoryInterface $entityRepository, AccountProxy $currentUser, EntityTypeManagerInterface $entity_manager) {
    $this->languageManager = $language_manager;
    $this->pathMatcher = $path_matcher;
    $this->request = $request_stack->getCurrentRequest();
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->uuidService = $uuid_service;
    $this->aliasManager = $alias_manager;
    $this->currentPath = $current_path;
    $this->entityRepository = $entityRepository;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entity_manager;
    $this->blockEntity = $this->entityTypeManager->getStorage('block');
  }

  /**
   * Method to do the presave block.
   */
  public function blockClassPreSave(&$entity) {

    // If there is no type, unset the Third Party Setting.
    if (empty($entity->getThirdPartySetting('dismissableblocks', 'type'))) {
      $entity->unsetThirdPartySetting('dismissableblocks', 'type');
    }

    // Get the config object.
    // $config = $this->configFactory->getEditable('block_class.settings');

    // Get the dismissable block type.
    $dismissable_block_type = $entity->getThirdPartySetting('dismissableblocks', 'type');

    if ($dismissable_block_type) {
      // Set the Third Party Settings.
      $entity->setThirdPartySetting('dismissableblocks', 'type', $dismissable_block_type);
    }

  }

  /**
   * Method to do the preprocess block.
   */
  public function preprocessBlock(&$variables) {

    // Blocks coming from page manager widget does not have id. If there is no
    // Block ID, skip that.
    if (empty($variables['elements']['#id'])) {
      return;
    }

    // If there is no block with this ID, skip.
    if (empty($this->blockEntity->load($variables['elements']['#id']))) {
      return;
    }

    // Load the block by ID.
    $block = $this->blockEntity->load($variables['elements']['#id']);

    // Add classes on block.
    $this->loadDismissableTypeOnBlock($block, $variables);

  }

  /**
   * Method to add classes on block.
   */
  public function loadDismissableTypeOnBlock(&$block, &$variables) {

    // Verify if the current block has Third Party Settings with classes.
    $type = $block->getThirdPartySetting('dismissableblocks', 'type');

    if (empty($type)) {
      return FALSE;
    }

    $variables['attributes']['class'][] = 'dismissableblocks';
    $variables['attributes']['dismissableblocks-id'][] = $block->id();
    $variables['#attached']['library'][] = 'dismissableblocks/dismiss_and_remember';

    if ($type == 'temporary') {
      $variables['attributes']['class'][] = 'dismissableblocks-temporary';
      $variables['#attached']['library'][] = 'dismissableblocks/dismiss_temporarily';
    }

    if ($type == 'cookie') {
      $variables['attributes']['class'][] = 'dismissableblocks-remember';
    }

    // @TODO absolutely replace this with a template
    $inline_template = <<<EOD
      <button type="button" class="button button--dismiss" title="Dismiss" ><span class="icon-close"></span>Close</button>
    EOD;

    // Title prefix might be more reliable as far as location, but less reliable at actually being there?
    // $variables['title_prefix']['dismissableblocks'] = ['#markup' => $markup,];
    $variables['content']['dismissableblocks'] = [
      '#type' => 'inline_template',
      '#template' => $inline_template,
      '#weight' => -999,
    ];

  }

  /**
   * Method to do a form alter.
   */
  public function configureBlockFormAlter(&$form, &$form_state) {

    // If the user don't have permission, bail.
    if (!$this->currentUser->hasPermission(DISMISSABLE_BLOCKS_PERMISSION)) {
      return;
    }

    $form_object = $form_state->getFormObject();

    // Implement the alter only if is a instance of EntityFormInterface.
    if (!($form_object instanceof EntityFormInterface)) {
      return;
    }

    // Put the default help text.
    $help_text = $this->t('Customize the styling of this block by adding CSS classes.');

    $form['dismissableblocks'] = [
      '#type' => 'details',
      '#title' => $this->t('Dismissable block'),
      '#open' => TRUE,
      '#description' => $help_text,
      // '#weight' => 0,
    ];

    /** @var \Drupal\block\BlockInterface $block */
    $block = $form_object->getEntity();

    // This will automatically be saved in the third party settings.
    $form['dismissableblocks']['third_party_settings']['#tree'] = TRUE;

    $options = [
      '' => $this->t('Block is not dismissable'),
      'temporary' => $this->t('Block is temporarily dismissable'),
      'cookie' => $this->t('Block stays dismissed if visitor agrees to cookie.'),
    ];
    $form['dismissableblocks']['third_party_settings']['dismissableblocks']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Dismissable block type'),
      '#options' => $options,
      '#description' => $this->t('Choose the type of dismissable block, if any.'),
      '#default_value' => $block->getThirdPartySetting('dismissableblocks', 'type'),
    ];

    $form['#validate'][] = 'dismissableblocks_form_block_form_validate';

  }

  /**
   * Method to do a form validate.
   */
  public function configureBlockFormValidate(&$form, &$form_state) {

    // Get the config object.
    // $config = $this->configFactory->getEditable('block_class.settings');

    // Validate class.
    $this->validateDismissableBlock($form, $form_state);

  }

  /**
   * Method to validate dismissable block type.
   */
  public function validateDismissableBlock(&$form, &$form_state) {

    // Get the ThirdPartySettings.
    $third_party_settings = $form_state->getValue('dismissableblocks')['third_party_settings'];

    // Clear empty values.
    // @TODO check that type is not an empty string
    if (!empty($third_party_settings['dismissableblocks'])) {
      $third_party_settings['dismissableblocks'] = array_filter($third_party_settings['dismissableblocks']);
    }

    // Merge with all third party settings.
    $all_third_party_settings = $form_state->getValue('third_party_settings');
    if (!empty($all_third_party_settings)) {
      $third_party_settings = array_merge($third_party_settings, $all_third_party_settings);
    }

    // Set the ThirdPartySettings with the default array.
    $form_state->setValue('third_party_settings', $third_party_settings);

    // Unset the old values.
    $form_state->unsetValue('dismissableblocks');

  }

}
