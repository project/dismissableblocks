!(function (Drupal, window, document) {
  "use strict";

  Drupal.behaviors.dismissableblocksTemporary = {
    attach: function (context, settings) {
      /**
       * Event handler to close notification. Only runs once.
       *
       * @param {Event} e
       */
      function handleClose(e) {
        e.preventDefault();
        const block = this.closest(".dismissableblocks-temporary");
        if (block) {
          block.classList.add("dismissed");
        }
        dismissBlock(block);
      }

      const $closeNotificationTriggers = once(
        "dismissableBlocksClose",
        ".dismissableblocks-temporary .button--dismiss",
        context
      );

      $closeNotificationTriggers.forEach((anchor) => {
        // These will run once.
        anchor.addEventListener("click", handleClose);
        anchor.addEventListener("touchstart", handleClose);
      });
    },
  };
})(Drupal, window, document);
