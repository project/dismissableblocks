!function(Drupal,window,document) {
  "use strict";

  Drupal.behaviors.dismissableblocksCookie = {
    attach: function (context, settings) {

      /**
       * Get a cookie.
       * @param {string} cname
       * @return {string|null}
       */
      const getCookie = function (cname) {
        return document.cookie.match('(^|;)\\s*' + cname + '\\s*=\\s*([^;]+)')?.pop() || null;
      }

      /**
       * Set a cookie.
       * @param {string} cname
       * @param {string} cvalue
       * @param {number} days
       */
      const setCookie = function(cname, cvalue, days) {
        const d = new Date();
        d.setTime(d.getTime() + (days * 24 * 3600000));
        const expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }

      /**
       * Close block functionality
       *
       * @param $block
       *   Dismissable block to be closed.
       */
      const dismissBlock = function ($block) {
        let blockId = $block.getAttribute('id'),
          cookieExpiration = 365, // days converted to seconds in function
          dismissedBlocks = getCookie('dismissableblocks');

        if (!dismissedBlocks) {
          dismissedBlocks = blockId;
        } else {
          dismissedBlocks = dismissedBlocks.split('.');
          if (!dismissedBlocks.includes(blockId)) {
            dismissedBlocks.push(blockId);
          }
          dismissedBlocks = dismissedBlocks.join('.');
        }
        // Only set cookie if people consent to disclaimer.
        // See web/core/modules/editor/js/editor.js for a core example of using
        // the dialog modal.
        const disclaimerModal = `<div class="message">This website will use a cookie to store your preference.</div>`;
        const dismissDialog = Drupal.dialog(disclaimerModal, {
          title: Drupal.t("Permanently dismiss announcement?"),
          classes: {
            'ui-dialog': 'dismissableblocks-disclaimer-modal',
          },
          width: 'auto',
          buttons: [
            {
              text: Drupal.t("Yes, keep hidden"),
              click() {
                $block.classList.add('dismissed');
                // Set accepted cookie with updated values.
                setCookie('dismissableblocks', dismissedBlocks, Number(cookieExpiration));
                dismissDialog.close();
                return false;
              }
            },
            {
              text: Drupal.t("No, close for now"),
              click() {
                $block.classList.add('dismissed');
                dismissDialog.close();
                return false;
              }
            }
          ]
        });
        dismissDialog.showModal();
      };

      /**
       * Event handler to close notification. Only runs once.
       *
       * @param {Event} e
       */
      function handleClose (e) {
        e.preventDefault();
        dismissBlock(this.closest(".dismissableblocks-remember"));
      }

      // Get block IDs that have already been dismissed and saved, if any.
      const dismissedBlocksCookie = getCookie('dismissableblocks') || '';
      const blockList = dismissedBlocksCookie.split('.');

      const $closeNotificationTriggers = once(
        "dismissableBlocksClose",
        ".dismissableblocks-remember .button--dismiss",
        context
      );
      $closeNotificationTriggers.forEach((anchor) => {
        blockList.forEach(blockId => {
          var block = context.getElementById(blockId);
          if (block) {
            block.classList.add('dismissed');
          }
        });
        // These will run once.
        anchor.addEventListener('click', handleClose);
        anchor.addEventListener('touchstart', handleClose);
      });


    }
  };

}(Drupal,window,document);
